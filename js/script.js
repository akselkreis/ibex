/* Author:
	First Name, Last Name:: iBec Creative :: Month Year
*/

var $ = jQuery.noConflict();

$(document).ready(function(){

	$('iframe').mediaWrapper();

	// Toggles for mobile menu
	$('.control').click(function(){
		$(this).toggleClass('active');
		$(this).children('.icon').toggleClass('active');
		$(this).siblings('.navigation.main').toggleClass('active');
	});

	$('.navigation .item .control').click(function(event){
		event.preventDefault();
		$(this).siblings('.navigation').toggleClass('active');
	});

	/*
		Prevents external links from changing the origin, which could lead to phishing attacks.
		Demo: http://codepen.io/akselkreis/pen/EgxPvA
	*/

	$('a[target="_blank"]:not([rel="noopener noreferrer"])').each(function(){
		$(this).attr('rel','noopener noreferrer').addClass('external');
	});

	//$( '#nav li:has(ul)' ).doubleTapToGo();

});


$(window).load(function() {

});
